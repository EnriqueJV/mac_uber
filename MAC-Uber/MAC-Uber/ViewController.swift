//
//  ViewController.swift
//  MAC-Uber
//
//  Created by Enrique on 11/8/19.
//  Copyright © 2019 Enrique. All rights reserved.
//

import UIKit
import FirebaseAuth
class ViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
       
        override func viewDidLoad() {
            super.viewDidLoad()

            // Do any additional setup after loading the view.
        }
        
        

        @IBAction func loginButtonPressed(_ sender: Any) {
            guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
            }
            
            
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error != nil{
                    self.presentAlertWith(title: "Error", message: error?.localizedDescription ?? "UPS An error ocurred")
                } else {
                    self.performSegue(withIdentifier: "loginSucccesSegue", sender: self)
                }
            }
        }
        

        @IBAction func registerButtonPressed(_ sender: Any) {
        }
    private func presentAlertWith(title:String, message: String){
        let alerControler = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAlertAction = UIAlertAction(title:"ok", style: .default){ _ in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
            self.emailTextField.becomeFirstResponder()
        }
        alerControler.addAction(okAlertAction)
        present(alerControler, animated: true, completion: nil)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    }
